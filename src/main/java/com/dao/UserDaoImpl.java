package com.dao;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Pets;
import com.model.User;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	SessionFactory sessionFactory;

	public void saveUser(User user) {

		sessionFactory.getCurrentSession().save(user);
	}

	public List<Pets> home() {
		return sessionFactory.getCurrentSession().createQuery("from Pets").list();

		
	}

	public void savePet(Pets pets) {
	 sessionFactory.getCurrentSession().save(pets);
		
		
	}

	

	public List<Pets> myPetList(int userId) {
		return sessionFactory.getCurrentSession().createSQLQuery("select * from Pets p where p.petownerid=:id")
				.setParameter("id", userId).list();

		
	}

	public User checkUser(String name) {
		return (User) sessionFactory.getCurrentSession()
				.createQuery("from User u where u.userName=:name")
				.setParameter("name", name).uniqueResult();
	}

	public void buyPet(int petId, int userId) {
		// get user
		User usr = sessionFactory.getCurrentSession().get(User.class, userId);
		// get pet
		Pets pet =  sessionFactory.getCurrentSession().get(Pets.class, petId);
		List pets = new LinkedList<>();
		pets.add(pet);
		pet.setUser(usr);
		usr.setPets(pets);

		// update in tables
		sessionFactory.getCurrentSession().save(pet);
		sessionFactory.getCurrentSession().save(usr);

	

	}

	public void setSessionFactory(SessionFactory mockedSessionFactory) {
		this.sessionFactory = mockedSessionFactory;
	}
}
