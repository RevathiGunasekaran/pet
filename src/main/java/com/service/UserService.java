package com.service;

import java.util.List;

import com.model.Pets;
import com.model.User;

public interface UserService {
	
	void saveUser(User user);

	List<Pets> home();

	List<Pets> myPetList(int id);

	void savePet(Pets pets);

	User checkUser(String name);

	void buyPet(int petId, int userId);

}
