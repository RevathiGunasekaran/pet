package com.model;

import javax.persistence.*;

@Entity
@Table(name = "pets")
public class Pets {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int petId;
	private String petName;
	@Column(name = "petAge", nullable = true)
	private int petAge;
	private String petPlace;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PETOWNERID")
	private User user;

	public Pets() {
		super();

	}

	public Pets(int petId, String petName, int petAge, String petPlace,User user) {
		super();
		this.petId = petId;
		this.petName = petName;
		this.petAge = petAge;
		this.petPlace = petPlace;
		this.user=user;
	}

	public Pets(String petName, int petAge, String petPlace,User user) {
		super();
		this.petName = petName;
		this.petAge = petAge;
		this.petPlace = petPlace;
		this.user=user;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

	public int getPetId() {
		return petId;
	}

	public void setPetId(int petId) {
		this.petId = petId;
	}

	public int getPetAge() {
		return petAge;
	}

	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
