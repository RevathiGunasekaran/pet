<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<title>Home Page</title>
</head>
<body>
	<!-- <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="./">WebSiteName</a>
    </div>
    
    <ul class="nav navbar-nav navbar-right">
      <li><a href="addPet"><span class="glyphicon glyphicon-user"></span> Add pet</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav> -->

	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">PetShop</a>
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="home">Home</a></li>
			<li><a href="myPets">My Pet</a></li>
			<li><a href="addPet">Add Pet</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="logout"><span
					class="glyphicon glyphicon-log-in"></span> Logout</a></li>
		</ul>
	</div>
	</nav>

	<div class="panel-body">
				<table class="table">
					<tr>
						
						<th>Pet Name</th>
						<th>Place</th>
						<th>Age</th>
						<th>Action</th>
					</tr>
					<c:forEach items="${petList}" var="pet" varStatus="petCounter">						
						<tr>
							<td><c:out value="${pet.petId}" />
							<td><c:out value="${pet.petName}" />
							<td><c:out value="${pet.petPlace}" />
							<td><c:out value="${pet.petAge}" />
							<td>
								
								<c:set  var="usr" value="${pet.user}" ></c:set> 
								<c:choose>
								    <c:when test="${empty usr}">
								    	
								    	 <button class="btn btn-primary">Buy</button>
								     </c:when>
								    <c:otherwise>
								    <button class="btn btn-primary"
                                         disabled="disabled" >Sold</button>
								    	
								     </c:otherwise>
								</c:choose>

							</td>
						</tr>
					</c:forEach>
				</table>
			</div>	
</body>
</html>