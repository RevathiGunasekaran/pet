<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Pet</title>
</head>
<body>
	<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="./">PetShop</a>
		</div>
		<ul class="nav navbar-nav">
			<li><a href="home">Home</a></li>
			<li><a href="myPets">My Pet</a></li>
			<li class="active"><a href="addPet">Add Pet</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="logout"><span class="glyphicon glyphicon-log-out"></span>
					Logout</a></li>
		</ul>
	</div>
	</nav>

	<center>
		<h2>Pet Information</h2>
		<form:form action="savePet" modelAttribute="pets">
			<table style="width: 50%; margin-left: 250px;">
				<tr>
					<td>Name</td>
					<td><form:input path="petName" />
				 <form:errors cssClass="error" path="petName"></form:errors></td>
				</tr>
				<tr>
					<td>Age</td>
					<td><form:input path="petAge" /></td>
				</tr>
				<tr>
					<td>Place</td>
					<td><form:input path="petPlace" /></td>
				</tr>
			<%--  <tr>
				<td>owner Id</td>
				<td><form:input path="PETOWNERID" /></td>
			</tr> --%>

				<tr>
					<td colspan="2"><input type="submit" value="Add Pet"
						class="btn btn-info"></td>
				</tr>
			</table>
		</form:form>
	</center>


</body>
</html>