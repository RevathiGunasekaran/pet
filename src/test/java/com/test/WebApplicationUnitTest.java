package com.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.SpringBootDependencyInjectionTestExecutionListener;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.ServletTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.main.MainAppController;
import com.model.Pets;
import com.model.User;
import com.service.UserService;

@SpringBootTest(classes = { TestBeanConfig.class })
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestExecutionListeners(listeners = { SpringBootDependencyInjectionTestExecutionListener.class,
		ServletTestExecutionListener.class })

public class WebApplicationUnitTest {
	@Mock
	UserService service;
	@InjectMocks
	MainAppController controller;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void indexTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/")).andExpect(MockMvcResultMatchers.status().isOk())

				.andExpect(MockMvcResultMatchers.view().name("Index"));
	}

	@Test
	public void loginTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/Login")).andExpect(MockMvcResultMatchers.status().isOk())

				.andExpect(MockMvcResultMatchers.view().name("LoginPage"));
	}

	@Test
	public void registerTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/Register")).andExpect(MockMvcResultMatchers.status().isOk())

				.andExpect(MockMvcResultMatchers.view().name("Registration"));
	}

	@Test
	public void addPetTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/addPet")).andExpect(MockMvcResultMatchers.status().isOk())

				.andExpect(MockMvcResultMatchers.view().name("AddPet"));
	}

	@Test
	public void logoutTest() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/logout")).andExpect(MockMvcResultMatchers.status().isOk())

				.andExpect(MockMvcResultMatchers.view().name("LoginPage"));
	}
	
	@Test
	public void getAllPets()throws Exception{
		User user = new User();
		user.setUserId(1);
		Pets pet1 = new Pets();
		pet1.setPetId(1);
		pet1.setPetPlace("chennai");
		pet1.setPetName("german shephard");
		pet1.setPetAge(2);
		pet1.setUser(user);
		Pets pet2 = new Pets();
		pet2.setPetId(1);
		pet2.setPetPlace("chennai");
		pet2.setPetName("shephard");
		pet2.setPetAge(2);
		pet2.setUser(user);
		List<Pets> ls = new ArrayList<Pets>();
		ls.add(pet1);
		ls.add(pet2);
		when(service.home()).thenReturn(ls);
	this.mockMvc.perform(MockMvcRequestBuilders.get("/home")).andExpect(MockMvcResultMatchers.status().isOk())
	
	.andExpect(MockMvcResultMatchers.view().name("HomePage"));
	}

	@Test
	public void saveUserTest() throws Exception {
		User user1 = new User();
		user1.setUserId(1);
		user1.setUserName("revathi");
		user1.setUserPasswd("bcd");
		user1.setConfirmUserPasswd("bcd");
		// when (service.saveUser(user1))
		this.mockMvc.perform(MockMvcRequestBuilders.post("/save")).andExpect(MockMvcResultMatchers.status().isFound());

				//.andExpect(MockMvcResultMatchers.view().name("redirect:/Login"));

	}

	@Test
	public void savePetsTest() throws Exception {

		User user1 = new User();
		user1.setUserId(1);
		Pets pets = new Pets();
		pets.setPetId(1);
		pets.setPetPlace("chennai");
		pets.setPetName("german shephard");
		pets.setPetAge(2);
		pets.setUser(user1);

		// when(service.savePet(pets)).thenReturn(pets);
		doNothing().when(service).savePet(pets);
		//verify(service, times(1)).savePet(pets);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/savePet").sessionAttr("id", 1))
				.andExpect(MockMvcResultMatchers.status().isFound());

		// .andExpect(MockMvcResultMatchers.view().name("redirect:/myPets"));
	}

	@Test
	public void getPetIdNotFound() throws Exception {

		User user = new User();
		user.setUserId(1000);

		when(service.myPetList(user.getUserId())).thenReturn(null);

		this.mockMvc.perform(MockMvcRequestBuilders.get("/myPets").sessionAttr("id", user.getUserId()))
				.andExpect(MockMvcResultMatchers.status().isOk());

		// .andExpect(MockMvcResultMatchers.view().name("Mypet"));

	}

	@Test
	public void buyPet() throws Exception {

		this.mockMvc.perform(MockMvcRequestBuilders.get("/buyPet").param("petId", "1").sessionAttr("id", 1))
				.andExpect(MockMvcResultMatchers.status().isFound());

		// .andExpect(MockMvcResultMatchers.view().name("redirect:/home"));

	}

	@Test
	public void getMyPetsTest() throws Exception {
		User user = new User();
		user.setUserId(1);
		Pets pet1 = new Pets();
		pet1.setPetId(1);
		pet1.setPetPlace("chennai");
		pet1.setPetName("german shephard");
		pet1.setPetAge(2);
		pet1.setUser(user);
		Pets pet2 = new Pets();
		pet2.setPetId(1);
		pet2.setPetPlace("chennai");
		pet2.setPetName("shephard");
		pet2.setPetAge(2);
		pet2.setUser(user);
		List<Pets> ls = new ArrayList<Pets>();
		ls.add(pet1);
		ls.add(pet2);
		when(service.myPetList(user.getUserId())).thenReturn(ls);

		this.mockMvc.perform(MockMvcRequestBuilders.get("/myPets").sessionAttr("id", user.getUserId()))
				.andExpect(MockMvcResultMatchers.status().isOk());

		// .andExpect(MockMvcResultMatchers.view().name("Mypet"));

	}

	@Test
	public void validateUserNull() throws Exception

	{
		User user = new User();
		user.setUserId(0);
		user.setUserName("abc");
		
		when(service.checkUser(user.getUserName())).thenReturn(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/checkUser"))
				.andExpect(MockMvcResultMatchers.status().isOk());

		// .andExpect(MockMvcResultMatchers.view().name("InvalidUser"));

	}

	@Test
	public void validateUserNotNull() throws Exception {
		User user = new User();
		user.setUserId(1);
		user.setUserName("revathi");
		user.setUserPasswd("revathi");

		when(service.checkUser(user.getUserName())).thenReturn(user);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/checkUser"))
				.andExpect(MockMvcResultMatchers.status().isOk());

		// .andExpect(MockMvcResultMatchers.view().name("redirect:/home"));

	}

}
